#!/bin/bash

# $ bash exec_ft_bert.sh [train OR output]

if [ $# -ne 1 ]; then
	echo "== USAGE: bash exec_ft_bert.sh [train OR output]"
	exit 1
fi

if [ ! -d ./data_bert/weekly/input_report ]; then
	mkdir -p ./data_bert/weekly/input_report
fi
if [ ! -d ./data_bert/weekly/raw ]; then
	mkdir -p ./data_bert/weekly/raw
fi
if [ ! -d ./data_bert/hourly/input_csv ]; then
	mkdir -p ./data_bert/hourly/input_csv
fi
if [ ! -d ./data_bert/hourly/output_csv ]; then
	mkdir -p ./data_bert/hourly/output_csv
fi
if [ ! -d ./data_bert/hourly/raw ]; then
	mkdir -p ./data_bert/hourly/raw
fi
if [ ! -d ./logs_bert/old ]; then
	mkdir -p ./logs_bert/old
fi
if [ ! -d ./resources/bert_model ]; then
	mkdir -p ./resources/bert_model
fi

if [ "$(ls ./*.log 2> /dev/null)" != '' ]; then
	echo "=== found old log file. move to './logs_bert/old/'."
	mv ./*.log ./logs_bert/old/
fi

echo "=== BEGIN $1"
python3.8 manage.py sample_bert $1 > ./out.log 2> ./error.log &
wait
echo "=== END   $1"

if [ "$(ls ./*.log 2> /dev/null)" != '' ]; then
	timeNow=`date +%Y-%m-%d_%H-%M-%S`
	echo "=== move log file to './logs_bert/${timeNow}/'."
	mkdir -p ./logs_bert/${timeNow}
	mv ./out.log ./logs_bert/${timeNow}/
	mv ./error.log ./logs_bert/${timeNow}/
fi

if [ $1 = "train" ]; then
	if [ -d ./resources/bert_model/tmp ]; then
		echo "=== move model directory to './resources/bert_model/${timeNow}/'"
		mv ./resources/bert_model/tmp ./resources/bert_model/${timeNow}
	fi
elif [ $1 = "output" ]; then
	if [ -d ./data_bert/hourly/out_result/tmp ]; then
		echo "=== move result directory to './data_bert/hourly/out_result/${timeNow}/'"
		mv ./data_bert/hourly/out_result/tmp ./data_bert/hourly/out_result/${timeNow}
	fi
fi
