# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db import connection
from image_model import ImageModel
from make_set_weekly import MakeSetWeekly
from make_set_hourly import MakeSetHourly
from upload_csv import UploadCsv
from tensorflow.python.platform import gfile
import glob
import csv
import datetime
import pytz

TRAIN = "train"
OUTPUT = "output"

MODEL_PATH = 'resources/model/sample.ckpt'
CATEGORY_NUM = 2

IMAGE_PATH_WEEKLY = 'train/img_weekly/raw/'
IMAGE_PATH_HOURLY = 'train/img_hourly/raw/'
OUTPUT_CSV_PATH = 'train/img_hourly/output_csv/'

class Command(BaseCommand):
    
    def add_arguments(self, parser):
        parser.add_argument('target')
        
    def handle(self, *args, **options):
        im = ImageModel(CATEGORY_NUM, MODEL_PATH)

        if options['target'] == TRAIN :
            msw = MakeSetWeekly()
            tmp = msw.main()
            if tmp == 1:
                return

            file_list_l = glob.glob(IMAGE_PATH_WEEKLY + 'low/*')
            file_list_h = glob.glob(IMAGE_PATH_WEEKLY + 'high/*')
            file_num = len(file_list_l) + len(file_list_h)

            image_path_list = []
            label_list = []
            image_path_list_test = []
            label_list_test = []

            image_path_list, label_list = self.set_image_and_label_list(glob.glob('{0}low/*'.format(IMAGE_PATH_WEEKLY)), 0, CATEGORY_NUM, image_path_list, label_list)
            image_path_list, label_list = self.set_image_and_label_list(glob.glob('{0}high/*'.format(IMAGE_PATH_WEEKLY,)), 1, CATEGORY_NUM, image_path_list, label_list)

            im.train_from_imagefile(image_path_list, label_list, file_num)
            
        if options['target'] == OUTPUT :
            path_s3_inputCsv = ''
            msh = MakeSetHourly()
            path_s3_inputCsv = msh.main()

            if path_s3_inputCsv == '' :
                print('=== not found CSVfile')
                return

            image_path_list = []
            
            self.set_image_list(glob.glob(IMAGE_PATH_HOURLY + '*'), image_path_list)

            ps = path_s3_inputCsv.split('-')
            filename_output = ps[0] + '-' + ps[1] + '-' + ps[2] + '-' + ps[3] + '-output.csv'
            f = open(OUTPUT_CSV_PATH + filename_output , 'w')
            csv_writer = csv.writer(f)
            
            name_list, output_list, y_conv_list = im.output_from_imagefile(image_path_list)
            csv_writer.writerow(['creative_part_id','raw_weight'])
            for ipl in image_path_list:
                for name, output, y_conv in zip(name_list, output_list, y_conv_list):
                    if ipl[0] == name:
                        res = [name, y_conv[-1]*2.0]
                        csv_writer.writerow(res)
                        break
                else:
                    res = [ipl[0], 1.0]
                    csv_writer.writerow(res)
            f.close()

            uc = UploadCsv()
            uc.main(OUTPUT_CSV_PATH + filename_output)

    def set_image_and_label_list(self, files, label, label_num, image_path_list, label_list):
        for k, file in enumerate(files):
            name = file.split("/")[-1]
            image_path_list.append([k, file])
            _label = [[0.0 for i in range(label_num)]]
            _label[0][label] = 1.0
            label_list.append(_label)
        return image_path_list, label_list

    def set_image_list(self, files, image_path_list):
        for file in files:
            name = file.split("/")[-1]
            image_path_list.append([name, file])
        return image_path_list
