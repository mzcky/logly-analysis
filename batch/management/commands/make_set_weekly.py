# python _devide_data.py

import os
import sys
import glob
import shutil
import csv
import datetime
import cv2
import requests
import numpy as np
import copy

DIR_INPUT_REPORT = 'train/img_weekly/input_report/'
DIR_DOWNLOAD_IMG = 'train/img_weekly/raw/'
PATH_BACKUP_LE = 'train/img_weekly/backup_le'

LIST_CAT = ['low','mid','high']
TRIM_CENTER = 40
TRIM_EDGE = 40
THRESH_DIFF = 32

FLAG_LOAD_FROM_BACKUP = 1

class MakeSetWeekly:

	def main(self):
		liner = []
		overLiner = []
		list_equal = [] # [parent, child]
		raw_img_size = []
		raw_img_center = []
		raw_img_edge = []

		path_report = self.find_latest_inputData()
		if path_report == '':
			print('=== NO REPORT FILE')
			return 1

		self.search_doubling(liner, path_report)
		if FLAG_LOAD_FROM_BACKUP == 0:
			self.download_imgs(liner)

		list_img = glob.glob(DIR_DOWNLOAD_IMG+'ALL/*')

		if FLAG_LOAD_FROM_BACKUP == 0:
			self.read_img(raw_img_size, raw_img_center, raw_img_edge, list_img)
			self.search_sameImg(list_equal, list_img, raw_img_size, raw_img_center, raw_img_edge)
			self.save_LE(list_equal)
		elif FLAG_LOAD_FROM_BACKUP == 1:
			self.load_LE(list_equal)

		self.combine_sameImg(list_equal, liner)
		list_equal = [] # free

		self.ignore_10kImps(overLiner, liner)
		liner = [] # free

		self.distribute_img(overLiner)
		self.drop_mid()

		return 0

	def calRateFromLine(self, line):
		imps = float(line[-3])
		clicks = float(line[-2])
		return (clicks / imps)

	def request_url(self, url, filename):
		response = requests.get(url)
		f = open(DIR_DOWNLOAD_IMG + 'ALL/' + filename , 'w')
		f.write(response.content)
		f.close()

	def find_latest_inputData(self):
		list_file = glob.glob(DIR_INPUT_REPORT+'*')
		list_file_appMTime = []

		for lf in list_file:
			list_file_appMTime.append([lf, os.path.getmtime(lf)])

		list_file_appMTime.sort(key=lambda x: x[1], reverse=True)
		try:
			path_report = list_file_appMTime[0][0]
			return list_file_appMTime[0][0]
		except:
			return ''


	def search_doubling(self, liner, path_report):
		f = open(path_report)
		csvF = csv.reader(f,delimiter='\t')

		for fLine in csvF:
			jpgUrl = fLine[-3].split('/')
			jpgName = jpgUrl[-4] + '-' + jpgUrl[-3] + '-' + jpgUrl[-2] + '-' + jpgUrl[-1]

			flag = 0
			for line in liner:
				if line[-1] == jpgName:
					flag = 1
					break
			if flag == 1:
				line[-3] = str(int(line[-3]) + int(fLine[-2]))
				line[-2] = str(int(line[-2]) + int(fLine[-1]))
				continue

			tmpLine = fLine
			tmpLine.append(jpgName)

			liner.append(tmpLine)
		f.close()
		
	def download_imgs(self, liner):
		print('=== BEGIN download imgs')
		sys.stdout.flush()

		try:
			shutil.rmtree(DIR_DOWNLOAD_IMG+'ALL/')
		except:
			pass
		try:
			os.makedirs(DIR_DOWNLOAD_IMG+'ALL/')
		except:
			pass

		for i,l in enumerate(liner):
			url = l[-4]
			filename = l[-1]
			if i % 100 == 0:
				print('=== {0} : {1} -> {2}'.format(i,url,filename))
				sys.stdout.flush()
			self.request_url(url, filename)

	def read_img(self, raw_img_size, raw_img_center, raw_img_edge, list_img):
		print('=== BEGIN read img')
		sys.stdout.flush()

		for path_img in list_img:
			try:
				tmp = cv2.imread(path_img)
			except:
				print('==== err: cant read: '+path_img.split('/')[-1])
				tmp = np.array([0,0,0])

			raw_img_center.append(copy.deepcopy(tmp))

		for i,img in enumerate(raw_img_center):
			try:
				tmp = img.shape
			except:
				print('==== err: cant get shape: '+list_img[i].split('/')[-1])
				tmp = (3,3,3)
			else:
				if img.shape[0] == 0:
					print('==== err: failed read: '+list_img[i].split('/')[-1])
			
			raw_img_size.append(copy.deepcopy(tmp))

			try:
				tmp = img[0:TRIM_EDGE, 0:TRIM_EDGE]
			except:
				print('==== err: cant trim edge: '+list_img[i].split('/')[-1])
				tmp = np.array([0,0,0])
			else:
				if tmp.shape[0] == 0:
					print('==== err: trim edge failed: '+list_img[i].split('/')[-1])
			
			raw_img_edge.append(copy.deepcopy(tmp))
			
			center_x = raw_img_size[i][0]/2
			center_y = raw_img_size[i][1]/2
			
			try:
				tmp = img[center_x-TRIM_CENTER/2 : center_x+TRIM_CENTER/2 , center_y-TRIM_CENTER/2 : center_y+TRIM_CENTER/2]
			except:
				print('==== err: cant trim center: '+list_img[i].split('/')[-1])
				tmp = np.array([0,0,0])
			else:
				if tmp.shape[0] == 0:
					print('==== err: trim edge failed: '+list_img[i].split('/')[-1])
			
			raw_img_center[i] = copy.deepcopy(tmp)

	def search_sameImg(self, list_equal, list_img, raw_img_size, raw_img_center, raw_img_edge):
		print('=== BEGIN search sameImg')
		sys.stdout.flush()

		for i,img1 in enumerate(list_img) :
			if i%10==0:
				print("i= "+str(i))
				sys.stdout.flush()

			if raw_img_size[i][0] == 3:
				continue

			j = i+1
			while j < len(list_img) :
				img2 = list_img[j]

				if raw_img_size[j][0] == 3:
					j+=1
					continue

				if img1 == img2 :
					j+=1
					continue

				if raw_img_size[i] != raw_img_size[j] :                       
					j+=1
					continue

				try:
					im_center_diff = raw_img_center[i].astype(int) - raw_img_center[j].astype(int)
					tmp = im_center_diff.max()
					im_center_diff_abs = np.abs(im_center_diff)
					tmp = im_center_diff_abs.max()

					im_edge_diff = raw_img_edge[i].astype(int) - raw_img_edge[j].astype(int)
					tmp = im_edge_diff.max()
					im_edge_diff_abs = np.abs(im_edge_diff)
					tmp = im_edge_diff_abs.max()

					if im_center_diff_abs.max() == 0 and im_edge_diff_abs.max() == 0 :
						list_equal.append([img1, list_img.pop(j)])
						tmp = raw_img_size.pop(j)
						tmp = raw_img_center.pop(j)
						tmp = raw_img_edge.pop(j)
						# no increment j
						continue
				except Exception as e:
					print(str(img1)+': size='+str(raw_img_center[i].shape))
					print(str(img2)+': size='+str(raw_img_center[j].shape))
					print('err: '+str(e))
					sys.stdout.flush()

				j+=1

		print('=== found '+str(len(list_equal)))
		sys.stdout.flush()

	def save_LE(self, list_equal):
		f = open(PATH_BACKUP_LE, 'w')
		for le in list_equal:
			f.write(str(le[0]) + ',' + str(le[1]) + '\n')
		f.close()
		
		print('=== list_equal is backedup to ' + PATH_BACKUP_LE)

	def load_LE(self, list_equal):
		print('=== BEGIN load LE')
		sys.stdout.flush()

		f = open(PATH_BACKUP_LE, 'r')
		for line in f:
			l = line.split(',')
			list_equal.append([l[0], l[1].replace('\n','').replace('\r','')])

	def combine_sameImg(self, list_equal, liner):
		print('=== BEGIN combine sameImg')
		sys.stdout.flush()

		for le in list_equal:
			parent = le[0].split('/')[-1]
			child = le[1].split('/')[-1]

			for li1 in liner:
				if li1[-1] != parent:
					continue
				
				for i,li2 in enumerate(liner):
					if li2[-1] != child:
						continue
					
					li1[-3] = str(int(li1[-3]) + int(li2[-3]))
					li1[-2] = str(int(li1[-2]) + int(li2[-2]))
					tmp = liner.pop(i)
					break
				break

	def ignore_10kImps(self, overLiner, liner):	
		# ignore imps < 10000
		for line in liner:
			if int(line[-3]) >= 10000:
				overLiner.append(line)
				continue

	def calc_thresholds(self, overLiner):
		rateList = []
		for l in overLiner:
			rate = self.calRateFromLine(l)
			rateList.append(rate)

		rateList.sort()
		num_LM = int(float(len(rateList))*1.0/3.0)
		num_MH = int(float(len(rateList))*2.0/3.0) 
		threshold_LM = rateList[num_LM]
		threshold_MH = rateList[num_MH]

		return threshold_LM, threshold_MH

	def distribute_img(self, overLiner):
		print('=== BEGIN distribute')
		sys.stdout.flush()

		try:
			for lc in LIST_CAT:
				shutil.rmtree(DIR_DOWNLOAD_IMG+lc)
		except:
			pass
		try:
			for lc in LIST_CAT:
				os.makedirs(DIR_DOWNLOAD_IMG+lc)
		except:
			pass
		
		threshold_LM, threshold_MH = self.calc_thresholds(overLiner)

		for i,l in enumerate(overLiner):
			rate = self.calRateFromLine(l)
			if rate <= threshold_LM:
				category = LIST_CAT[0]
			elif rate <= threshold_MH:
				category = LIST_CAT[1]
			else:
				category = LIST_CAT[2]

			filename = l[-1]
			try:
				shutil.move(DIR_DOWNLOAD_IMG+'ALL/'+filename, DIR_DOWNLOAD_IMG+category+'/')
			except:
				pass

	def drop_mid(self):
		shutil.rmtree(DIR_DOWNLOAD_IMG+LIST_CAT[1])

