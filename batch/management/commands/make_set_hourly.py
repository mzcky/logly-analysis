# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
import os
import sys
import glob
import shutil
import csv
import datetime
import pytz
import requests
import boto3

DIR_INPUT_CSV = 'train/img_hourly/input_csv'
DIR_DOWNLOAD_IMG = 'train/img_hourly/raw'
NAME_S3BUCKET = 'intentad-sharaq'

class MakeSetHourly:

	def main(self):
		path_reports = self.download_csv()
		if path_reports == '' :
			return ''

		list_data = self.read_reports(path_reports)
		self.download_imgs(list_data)
		
		return path_reports.split('/')[-1]

	def download_csv(self):
		s3 = boto3.resource('s3')
		bucket = s3.Bucket(NAME_S3BUCKET)

		dt_now = datetime.datetime.now(pytz.timezone('Asia/Tokyo'))
		dt_2before = dt_now - datetime.timedelta(hours=2)
		str_dt = dt_2before.strftime('%Y/%m/%d/%H')

		path_s3_inputCsv = 'v1/input/' + str_dt + '/input.csv'
		ps = str_dt.split('/')
		filename = ps[0] + '-' + ps[1] + '-' + ps[2] + '-' + ps[3] + '-input.csv'
		try:
			bucket.download_file(path_s3_inputCsv, DIR_INPUT_CSV + '/' + filename)
		except:
			print('=== failed downloading [' + path_s3_inputCsv + ']')
			return ''
		else:
			print('=== succeeded downloading [' + path_s3_inputCsv + '] to [' + DIR_INPUT_CSV + '/' + filename + ']')

		return (DIR_INPUT_CSV + '/' + filename)

	def read_reports(self, path_reports):
		list_data = []
		
		f = open(path_reports, 'r')
		csvF = csv.reader(f,delimiter=',')
		next(csvF) # skip header

		for fLine in csvF:
			list_data.append(fLine)

		f.close()
	
		return list_data

	def download_imgs(self, list_data):
		shutil.rmtree(DIR_DOWNLOAD_IMG + '/')
		os.makedirs(DIR_DOWNLOAD_IMG)

		for i,fLine in enumerate(list_data):
			if i%100==0:
				print('=== i= '+str(i))
				sys.stdout.flush()
			url = fLine[-1]
			filename = fLine[0]

			response = requests.get(url)
			f = open(DIR_DOWNLOAD_IMG + '/' + filename ,'w')
			f.write(response.content)
			f.close()

