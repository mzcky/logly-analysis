# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
import boto3

NAME_S3BUCKET = 'intentad-sharaq'

class UploadCsv:
	
	def main(self, path_output_csv):
		self.upload_csv(path_output_csv)
	
	def upload_csv(self, path_output_csv):
		s3 = boto3.resource('s3')
		bucket = s3.Bucket(NAME_S3BUCKET)

		fn = path_output_csv.split('/')[-1]
		path_s3 = 'v1/output/' + fn.replace('-', '/')
		try:
			bucket.upload_file(path_output_csv, path_s3)
			s3.Object(NAME_S3BUCKET, path_s3).load()
		except:
			print('=== failed upload')
		else:
			print('=== succeeded uploading to [' + path_s3 + ']')
