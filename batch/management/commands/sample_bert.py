# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db import connection
from batch.management.commands.run_glue import Run_Glue
import os
import sys
import glob
from datetime import datetime, timedelta, timezone
import json

TRAIN = 'train'
OUTPUT = 'output'

CONFIG_TRAIN = 'data_bert/_config/train.json'
CONFIG_OUTPUT_DIR = 'data_bert/_config/'
CONFIG_OUTPUT_TMP = 'data_bert/_config/output_template.json'
MODEL_DIR = 'resources/bert_model/'

class Command(BaseCommand):
    
    def add_arguments(self, parser):
        parser.add_argument('target')
        
    def handle(self, *args, **options):

        if options['target'] == TRAIN :
            run_glue = Run_Glue()
            run_glue.main(json_file=CONFIG_TRAIN)
            
        if options['target'] == OUTPUT :
            path_model = self.get_latest_model_path()
            strDt = path_model.split('/')[-1]
            json_file_name = strDt + '.json'

            f = open(CONFIG_OUTPUT_TMP, 'r')
            jsonCO = json.load(f)
            f.close()

            jsonCO['model_name_or_path'] = path_model
            f = open(CONFIG_OUTPUT_DIR + json_file_name, 'w')
            json.dump(jsonCO, f, indent=4)
            f.close()

            run_glue = Run_Glue()
            run_glue.main(json_file=CONFIG_OUTPUT_DIR + json_file_name)

    def get_latest_model_path(self):
        list_model = glob.glob(MODEL_DIR+'*')
        list_model_appMTime = []

        for lm in list_model: 
            list_model_appMTime.append([lm, os.path.getmtime(lm)])

        list_model_appMTime.sort(key=lambda x: x[1], reverse=True)

        try:
            return list_model_appMTime[0][0]
        except:
            return ''

