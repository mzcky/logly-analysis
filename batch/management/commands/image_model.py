# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
import tensorflow as tf
import numpy
from PIL import Image
import glob
import os.path
from tensorflow.python.platform import gfile
from django.db import connection, transaction
import urllib
import traceback
import sys

INPUT_DIM = 2048
BATCH_SIZE = 10
TEST_SIZE_RATIO = 0.2

class ImageModel:
    def __init__(self, output_dim, model_path):
        self.output_dim = output_dim
        self.model_path = model_path
    
    def train_from_imagefile(self, image_path_list, label_list,  file_num):
        self.__train(image_path_list, label_list, file_num)
    def output_from_imagefile(self, image_path_list):
        id_list, output_list, y_conv_list = self.__output(image_path_list)
        return id_list, output_list, y_conv_list
   
    def __train(self, image_path_list, label_list, file_num):
        loop_range_num = file_num

        x = tf.placeholder("float", shape=[None, INPUT_DIM]) 
        y_ = tf.placeholder("float", shape=[None, self.output_dim]) # output
        keep_prob = tf.placeholder("float")
        
        W_fc1 = self.__weight_variable([2048, 500]) # image size has been reduced to 4x4, fully connected 6400 neurons (10*10*64=6400)
        b_fc1 = self.__bias_variable([500])
        h_fc1 = tf.nn.relu6(tf.matmul(x, W_fc1) + b_fc1)
        
        h_fc_2_drop = tf.nn.dropout(h_fc1, keep_prob) # dropout
        W_fc_2 = self.__weight_variable([500, self.output_dim])
        b_fc_2 = self.__bias_variable([self.output_dim])
        y_conv=tf.nn.softmax(tf.matmul(h_fc_2_drop, W_fc_2) + b_fc_2) # softmax
        cross_entropy = -tf.reduce_sum(y_*tf.log(y_conv))
        train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
        prediction = tf.argmax(y_conv,1)
        actual = tf.argmax(y_,1)
        correct_prediction = tf.equal(prediction, actual)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        saver = tf.train.Saver()

        with tf.Session() as sess:
            train_data, train_label, test_data, test_label = self.__input_data(sess, image_path_list, label_list)
            sess.run(tf.initialize_all_variables())

            for i in range(loop_range_num):
                batch = self.__mini_batch(train_data, train_label, i)

                if i%100 == 0:
                    train_accuracy = accuracy.eval(feed_dict={x:batch[0], y_: batch[1], keep_prob: 1.0})
                    print "step %d, training accuracy %g"%(i, train_accuracy)
                    sys.stdout.flush()
                train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
            
            saver.save(sess, self.model_path)
                
            print "test accuracy %g"%accuracy.eval(feed_dict={x: test_data, y_: test_label, keep_prob: 1.0})
   
    def __input_data(self, sess, image_path_list, label_list): 
        self.__create_graph() 

        data_list = None
        trans_label_list = []
        for idx, (image_path, label) in enumerate(zip(image_path_list, label_list)):
            if idx % 100 == 0:
                print str(idx) + "/" + str(len(image_path_list))
                sys.stdout.flush()
            data_list, trans_label_list = self.__get_learning_images(sess, image_path, tf.constant(label), data_list, trans_label_list)

        # rearrange
        data_list = data_list.eval()
        trans_label_list = trans_label_list.eval()
        perm = numpy.arange(trans_label_list.shape[0])
        numpy.random.shuffle(perm)
        data_list = data_list[perm]
        trans_label_list = trans_label_list[perm]

        # divide
        test_size = int(len(data_list) * TEST_SIZE_RATIO)
        test_data_list = data_list[:test_size]
        test_label_list = trans_label_list[:test_size]
        train_data_list = data_list[test_size:]
        train_label_list = trans_label_list[test_size:]
 
        return train_data_list, train_label_list, test_data_list, test_label_list
    
    def __get_learning_images(self, sess, image, label, data_list, label_list):
        try:
            pool_3_tensor = sess.graph.get_tensor_by_name('pool_3:0')
            image_data = gfile.FastGFile(glob.glob(image[1])[0]).read()
            pool_3_vector = sess.run(pool_3_tensor,{'DecodeJpeg/contents:0': image_data})

            if data_list is not None:
                data_list = tf.concat([data_list, tf.reshape(pool_3_vector, [-1, INPUT_DIM])], 0)
                label_list = tf.concat([label_list, tf.reshape(label, [-1, self.output_dim])], 0)                
            else:
                data_list = tf.reshape(pool_3_vector, [-1, INPUT_DIM])
                label_list = tf.reshape(label, [-1, self.output_dim])
                
        except:
            print traceback.format_exc()
            return data_list, label_list
            
        return data_list, label_list
    
    def __output(self, image_path_list):
        if not image_path_list or len(image_path_list) == 0:
            return False

        # initialize
        x = tf.placeholder("float", shape=[None, INPUT_DIM]) 
        y_ = tf.placeholder("float", shape=[None, self.output_dim]) # output
        keep_prob = tf.placeholder("float")
        
        W_fc1 = self.__weight_variable([2048, 500]) # image size has been reduced to 4x4, fully connected 6400 neurons (10*10*64=6400)
        b_fc1 = self.__bias_variable([500])
        h_fc1 = tf.nn.relu6(tf.matmul(x, W_fc1) + b_fc1)
        
        h_fc_2_drop = tf.nn.dropout(h_fc1, keep_prob) # dropout
        W_fc_2 = self.__weight_variable([500, self.output_dim])
        b_fc_2 = self.__bias_variable([self.output_dim])
        y_conv=tf.nn.softmax(tf.matmul(h_fc_2_drop, W_fc_2) + b_fc_2) # softmax

        saver = tf.train.Saver()
        with tf.Session() as sess:
            self.__create_graph() 
            sess.run(tf.initialize_all_variables())
            saver.restore(sess, self.model_path)
            id_list, image_list = self.__get_output_images(sess, image_path_list)
            y_conv_list = y_conv.eval(feed_dict={x: image_list, keep_prob: 1.0})   
        output_list = []
        for yc in y_conv_list:
            yc = yc.tolist()
            output_list.append(yc.index(max(yc))) 
        return id_list, output_list, y_conv_list
    
    def __get_output_images(self, sess, image_urls):
        image_list = None
        id_list = []

        print('=== loading img')
        for idx, image in enumerate(image_urls):
            # image[0] : id
            # image[1] : url
            if idx % 100 == 0:
                print('==== idx= '+str(idx))
                sys.stdout.flush()
            try:
                pool_3_tensor = sess.graph.get_tensor_by_name('pool_3:0')
                image_data = gfile.FastGFile(image[1]).read()
                pool_3_vector = sess.run(pool_3_tensor,{'DecodeJpeg/contents:0': image_data})
                
                if image_list is not None:
                    image_list = tf.concat([image_list, tf.reshape(pool_3_vector, [-1, INPUT_DIM])], 0)
                else:
                    image_list = tf.reshape(pool_3_vector, [-1, INPUT_DIM])
                    
                id_list.append(image[0])
            except:
                print traceback.format_exc()
                continue
        return id_list, image_list.eval()
    
    def __create_graph(self):
        with gfile.FastGFile(os.path.join('resources/imagenet', 'classify_image_graph_def.pb'), 'r') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')
    
    def __weight_variable(self,shape):
        initial = tf.truncated_normal(shape, stddev=0.1)
        return tf.Variable(initial)
    
    def __bias_variable(self,shape):
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial)
    
    # __mini_batch
    def __mini_batch(self, data, label, i):
        index = (i * BATCH_SIZE) % data.shape[0]
        return data[index : index + BATCH_SIZE], label[index:index+BATCH_SIZE]
