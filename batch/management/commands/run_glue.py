# coding=utf-8
""" Finetuning the library models for sequence classification on GLUE (Bert, XLM, XLNet, RoBERTa, Albert, XLM-RoBERTa)."""

from django.core.management.base import BaseCommand, CommandError
import dataclasses
import logging
import os
import sys
import copy
from dataclasses import dataclass, field
from typing import Callable, Dict, Optional

import numpy as np

from transformers import AutoConfig, AutoModelForSequenceClassification, AutoTokenizer, EvalPrediction, GlueDataset
from transformers import GlueDataTrainingArguments as DataTrainingArguments
from transformers import (
    HfArgumentParser,
    Trainer,
    TrainingArguments,
    glue_compute_metrics,
    glue_output_modes,
    glue_tasks_num_labels,
    set_seed,
)

@dataclass
class ModelArguments:
		model_name_or_path: str = field(
				metadata={"help": "Path to pretrained model or model identifier from huggingface.co/models"}
		)
		config_name: Optional[str] = field(
				default=None, metadata={"help": "Pretrained config name or path if not the same as model_name"}
		)
		tokenizer_name: Optional[str] = field(
				default=None, metadata={"help": "Pretrained tokenizer name or path if not the same as model_name"}
		)
		cache_dir: Optional[str] = field(
				default=None, metadata={"help": "Where do you want to store the pretrained models downloaded from s3"}
		)


class Run_Glue:

	logger = logging.getLogger(__name__)

	def main(self, json_file):

			parser = HfArgumentParser((ModelArguments, DataTrainingArguments, TrainingArguments))

			model_args, data_args, training_args = parser.parse_json_file(json_file=os.path.abspath(json_file))

			if (
					os.path.exists(training_args.output_dir)
					and os.listdir(training_args.output_dir)
					and training_args.do_train
					and not training_args.overwrite_output_dir
			):
					raise ValueError(
							f"Output directory ({training_args.output_dir}) already exists and is not empty. Use --overwrite_output_dir to overcome."
					)

			# Setup logging
			logging.basicConfig(
					format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
					datefmt="%m/%d/%Y %H:%M:%S",
					level=logging.INFO if training_args.local_rank in [-1, 0] else logging.WARN,
			)
			self.logger.warning(
					"Process rank: %s, device: %s, n_gpu: %s, distributed training: %s, 16-bits training: %s",
					training_args.local_rank,
					training_args.device,
					training_args.n_gpu,
					bool(training_args.local_rank != -1),
					training_args.fp16,
			)
			self.logger.info("Training/evaluation parameters %s", training_args)

			set_seed(training_args.seed)

			try:
					num_labels = glue_tasks_num_labels[data_args.task_name]
					output_mode = glue_output_modes[data_args.task_name]
			except KeyError:
					raise ValueError("Task not found: %s" % (data_args.task_name))

			# Load pretrained model and tokenizer
			#
			# Distributed training:
			# The .from_pretrained methods guarantee that only one local process can concurrently
			# download model & vocab.

			print('=== Load pretrained model and tokenizer')

			config = AutoConfig.from_pretrained(
					model_args.config_name if model_args.config_name else model_args.model_name_or_path,
					num_labels=num_labels,
					finetuning_task=data_args.task_name,
					cache_dir=model_args.cache_dir,
			)
			tokenizer = AutoTokenizer.from_pretrained(
					model_args.tokenizer_name if model_args.tokenizer_name else model_args.model_name_or_path,
					cache_dir=model_args.cache_dir,
			)
			model = AutoModelForSequenceClassification.from_pretrained(
					model_args.model_name_or_path,
					from_tf=bool(".ckpt" in model_args.model_name_or_path),
					config=config,
					cache_dir=model_args.cache_dir,
			)

			# Get datasets
			print('=== Get datasets')

			train_dataset = (
					GlueDataset(data_args, tokenizer=tokenizer) 
					if training_args.do_train
					else None
			)
			eval_dataset = (
					GlueDataset(data_args, tokenizer=tokenizer, mode="dev")
					if training_args.do_eval
					else None
			)
			test_dataset = (
					GlueDataset(data_args, tokenizer=tokenizer, mode="test")
					if training_args.do_predict
					else None
			)

			def build_compute_metrics_fn(task_name: str) -> Callable[[EvalPrediction], Dict]:
					def compute_metrics_fn(p: EvalPrediction):
							if output_mode == "classification":
									preds = np.argmax(p.predictions, axis=1)
							elif output_mode == "regression":
									preds = np.squeeze(p.predictions)
							return glue_compute_metrics(task_name, preds, p.label_ids)

					return compute_metrics_fn

			# Initialize our Trainer
			print('=== Initialize our Trainer')

			trainer = Trainer(
					model=model,
					args=training_args,
					train_dataset=train_dataset,
					eval_dataset=eval_dataset,
					compute_metrics=build_compute_metrics_fn(data_args.task_name),
			)

			# Training
			print('=== Training')

			if training_args.do_train:
					trainer.train(
							model_path=model_args.model_name_or_path if os.path.isdir(model_args.model_name_or_path) else None
					)
					trainer.save_model()
					if trainer.is_world_master():
							tokenizer.save_pretrained(training_args.output_dir)

			# Evaluation
			print('=== Evaluation')

			eval_results = {}
			if training_args.do_eval:
					self.logger.info("*** Evaluate ***")

					eval_datasets = [eval_dataset]

					for eval_dataset in eval_datasets:
							trainer.compute_metrics = build_compute_metrics_fn(eval_dataset.args.task_name)
							eval_result = trainer.evaluate(eval_dataset=eval_dataset)

							output_eval_file = os.path.join(
									training_args.output_dir, f"eval_results_{eval_dataset.args.task_name}.txt"
							)
							if trainer.is_world_master():
									with open(output_eval_file, "w") as writer:
											self.logger.info("***** Eval results {} *****".format(eval_dataset.args.task_name))
											for key, value in eval_result.items():
													self.logger.info("  %s = %s", key, value)
													writer.write("%s = %s\n" % (key, value))

							eval_results.update(eval_result)

			# Prediction
			if training_args.do_predict:
					logging.info("*** Test ***")
					test_datasets = [test_dataset]

					for test_dataset in test_datasets:
							predictions = trainer.predict(test_dataset=test_dataset).predictions
							if output_mode == "classification":
									predictions_raw = copy.deepcopy(predictions)
									predictions = np.argmax(predictions, axis=1)

							output_test_file = os.path.join(
									training_args.output_dir, f"test_results_{test_dataset.args.task_name}.txt"
							)
							if trainer.is_world_master():
									with open(output_test_file, "w") as writer:
											self.logger.info("***** Test results {} *****".format(test_dataset.args.task_name))
											writer.write("index\tprediction\n")
											for index, item in enumerate(predictions):
													if output_mode == "regression":
															writer.write("%d\t%3.3f\n" % (index, item))
													else:
															item = test_dataset.get_labels()[item]
															writer.write("%d\t%s\n" % (index, item))

			if training_args.do_predict:
				with open('./data_bert/hourly/out_result/tmp/output.tsv', 'w') as writer:
						try:
								# writer.write('index\tlogits_label_0\tlogits_label_1\tprob_label_0\tprob_label_1\n')
								writer.write('index\traw_weight\n')

								for index, logits in enumerate(predictions_raw):
										probs = np.exp(logits) / np.sum(np.exp(logits), axis=0)
										# writer.write('%d\t%f\t%f\t%f\t%f\n' % (index, logits[0], logits[1], probs[0], probs[1]))
										writer.write('%d\t%f\n' % (index, probs[1]*2.0))
						except Exception as e:
								print(e)

			print('=== END run_glue.py main()')
			return eval_results

