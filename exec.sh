#!/bin/bash

# $ bash exec.sh [train OR output]

if [ $# -ne 1 ]; then
	echo "== USAGE: bash exec.sh [train OR output]"
	exit 1
fi

if [ ! -d ./train/img_weekly/input_report ]; then
	mkdir -p train/img_weekly/input_report
fi
if [ ! -d ./train/img_weekly/raw ]; then
	mkdir -p train/img_weekly/raw
fi
if [ ! -d ./train/img_hourly/input_csv ]; then
	mkdir -p train/img_hourly/input_csv
fi
if [ ! -d ./train/img_hourly/output_csv ]; then
	mkdir -p train/img_hourly/output_csv
fi
if [ ! -d ./train/img_hourly/raw ]; then
	mkdir -p train/img_hourly/raw
fi
if [ ! -d ./logs/old ]; then
	mkdir -p logs/old
fi

if [ "$(ls ./*.log 2> /dev/null)" != '' ]; then
	echo "=== found old log file. move to './logs/old/'."
	mv ./*.log ./logs/old/
fi

echo "=== BEGIN $1"
python2.7 manage.py sample $1 > out.log 2> error.log &
wait
echo "=== END   $1"

if [ "$(ls ./*.log 2> /dev/null)" != '' ]; then
	timeNow=`date +%Y-%m-%d_%H-%M-%S`
	echo "=== move log file to './logs/${timeNow}/'."
	mkdir -p logs/${timeNow}
	mv ./out.log ./logs/${timeNow}/
	mv ./error.log ./logs/${timeNow}
fi

